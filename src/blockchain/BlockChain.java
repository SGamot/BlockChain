package blockchain;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.LinkedList;

public class BlockChain
{
  LinkedList<Block> blockchain = new LinkedList<>();
  
  private class Block
  {
  
    String data;
    String previousHash;
    String hash;
    int nonce;
    long timestamp;
  
    public Block(String data, String previousHash)
    {
      this.data = data;
      this.previousHash = previousHash;
      this.hash = hash();
      this.timestamp = System.currentTimeMillis()/1000L; // A unix timestamp.
    }
    
    public String hash()
    {
      hash = DigestUtils.sha256Hex(String.valueOf(timestamp)+ String.valueOf(nonce) + previousHash+data);
      return hash;
    }
    
    public void mineBlock(int difficulty)
    {
      StringBuffer buf = new StringBuffer();
      for(int i=0;i<difficulty;i++)
      {
        buf.append(0);
      }
     String b = buf.toString();
      while(!b.equals(hash.substring(0,difficulty)))
      {
        nonce++;
        hash = hash();
      }
      
      System.out.println("Block Mined: " + hash);
    }
  
    @Override public String toString()
    {
      return "\nBlock{\n" + "\tdata='" + data + '\'' + " \n\tpreviousHash='"
              + previousHash + '\'' + "\n\thash='" + hash + '\'' + "\n\ttimestamp="
              + timestamp + "\n}";
    }
  }
  
  public BlockChain()
  {
    Block block = new Block("Genesis Block" ,"0");
    block.hash = block.hash();
    blockchain.add(block);
  }
  
  public void addBlock(String data)
  {
    Block block = new Block(data,blockchain.getLast().hash);
    block.mineBlock(5);
    blockchain.add(block);
  }
  
  public boolean checkIntegrity()
  {
    for(int i=1; i<blockchain.size()-1; i++)
    {
      Block current = blockchain.get(i);
      Block prev = blockchain.get(i-1);
      
      if(!current.hash.equals(current.hash()))
          return false;
      if(!prev.hash .equals(current.previousHash))
      {
        return false;
      }
    }
    return true;
  }
  @Override public String toString()
  {
    return "BlockChain{" + "blockchain=" + blockchain + '}';
  }
}
